package com.javabasics.repository;

import com.javabasics.repository.entity.TaskEntity;

import java.util.List;

public interface TaskDao {
    Long save(TaskEntity taskEntity);
    TaskEntity findById(Long id);
    List<TaskEntity> findByUserId(Long userId);
}
