package com.javabasics.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    public static Connection getConnection(){
        Connection connection;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Driver was not found");
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/todolist","root","hhizp5565");
        } catch (SQLException e) {
            throw new RuntimeException("Connection was not created");
        }

        return connection;
    }
}
