package com.javabasics.repository;

import com.javabasics.connection.ConnectionFactory;
import com.javabasics.repository.entity.UserEntity;

import java.sql.*;

public class JdbcUserDao implements UserDao {
    private Connection connection = ConnectionFactory.getConnection();

    @Override
    public Long save(UserEntity userEntity) {
        PreparedStatement ps = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            ps = connection.prepareStatement("insert into user(name,password) values(?,?)");
            ps.setString(1, userEntity.name);
            ps.setString(2, userEntity.password);
            ps.execute();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select LAST_INSERT_ID() as id");
            resultSet.next();
            return resultSet.getLong("id");

        } catch (SQLException e) {
            throw new RuntimeException();
        } finally {
            try {
                statement.close();
                ps.close();
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public UserEntity findById(Long id) {

        UserEntity userEntity = new UserEntity();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from user where id =" + id);
            while (resultSet.next()) {
                userEntity.id = resultSet.getLong("id");
                userEntity.name = resultSet.getString("name");
                userEntity.password = resultSet.getString("password");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userEntity;
    }

    @Override
    public UserEntity findByNameAndPassword(String name, String password) {
        UserEntity userEntity = new UserEntity();
        PreparedStatement ps =null;
        try {
            ps = connection.prepareStatement("select * from user where name = ? and password = ?");
            ps.setString(1, name);
            ps.setString(2, password);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                userEntity.id = resultSet.getLong("id");
                userEntity.name = resultSet.getString("name");
                userEntity.password = resultSet.getString("password");

            }
            ps.close();
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userEntity;
    }
}
