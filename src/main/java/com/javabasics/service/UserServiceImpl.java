package com.javabasics.service;

import com.javabasics.repository.TaskDao;
import com.javabasics.repository.UserDao;
import com.javabasics.repository.entity.UserEntity;
import com.javabasics.service.model.User;
import com.javabasics.service.user.UserService;

public class UserServiceImpl implements UserService {
    private UserDao userDao;



    public UserServiceImpl(UserDao userDao){
        this.userDao = userDao;
    }



    @Override
    public Long save(User user) {
        return userDao.save(convertToUserEntity(user));
    }

    private UserEntity convertToUserEntity(User user) {
        UserEntity userEntity = new UserEntity();
        userEntity.name=user.name;
        userEntity.password=user.password;
        return userEntity;
    }

    @Override
    public User findById(Long id) {
        return convertToUser(userDao.findById(id));
    }

    @Override
    public User findByNameAndPassword(String name, String password) {
        return convertToUser(userDao.findByNameAndPassword(name, password));
    }

    private User convertToUser(UserEntity userEntity) {
        User user = new User();
        user.name=userEntity.name;
        user.id=userEntity.id;
        user.password=userEntity.password;

        return user;
    }

}
