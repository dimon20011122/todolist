package com.javabasics.repository;

import com.javabasics.repository.entity.UserEntity;

import java.util.HashMap;
import java.util.Map;

public class JpaUserDao extends GenericDao<UserEntity> implements UserDao {
    @Override
    public UserEntity findByNameAndPassword(String name, String password) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        params.put("password", password);
        return findByParameters(params).get(0);
    }
}
