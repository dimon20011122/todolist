package com.javabasics.repository;

import com.javabasics.connection.ConnectionFactory;
import com.javabasics.repository.entity.RoleEntity;
import com.javabasics.repository.entity.TaskEntity;

import java.sql.*;

public class JdbcRoleDao implements RoleDao {
    private Connection connection = ConnectionFactory.getConnection();

    @Override
    public Long save(RoleEntity roleEntity) {
        PreparedStatement ps = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            ps = connection.prepareStatement("insert into user(name) values(?)");
            ps.setString(1, roleEntity.name);
            ps.execute();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select LAST_INSERT_ID() as id");
            resultSet.next();
            return resultSet.getLong("id");

        } catch (SQLException e) {
            throw new RuntimeException();
        } finally {
            try {
                statement.close();
                ps.close();
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }

    @Override


    public RoleEntity findByRoleId(Long id) {
        RoleEntity roleEntity = new RoleEntity();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from user where id =" + id);
            while (resultSet.next()) {
                roleEntity.id = resultSet.getLong("id");
                roleEntity.name = resultSet.getString("name");
            }
            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return roleEntity;
    }
    }

