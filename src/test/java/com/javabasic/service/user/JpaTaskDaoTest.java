package com.javabasic.service.user;

import com.javabasics.repository.*;
import com.javabasics.repository.entity.TaskEntity;
import com.javabasics.repository.entity.UserEntity;
import com.javabasics.service.TaskService;
import com.javabasics.service.TaskServiceImpl;
import com.javabasics.service.UserServiceImpl;
import com.javabasics.service.model.Task;
import com.javabasics.service.model.User;
import com.javabasics.service.user.UserService;
import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class JpaTaskDaoTest {
    private TaskDao taskDao = new JpaTaskDao();
    private UserDao userDao = new JpaUserDao();


    @Test
    public void JpaTaskDaoIsSaving() {
        TaskEntity task = new TaskEntity();
        UserEntity user = new UserEntity();
        user.name = "ivan" + System.currentTimeMillis();
        user.password = "pass";
        Long id = userDao.save(user);
        task.name = "ivansTask" + System.currentTimeMillis();
        task.userId = id;
        Long taskId = taskDao.save(task);
        assertTrue(taskId != null && taskId != 0L);

    }
    @Test
    public void JpaTaskDaoIsFindingById() {
        TaskEntity task = new TaskEntity();
        UserEntity user = new UserEntity();
        user.name = "ivan" + System.currentTimeMillis();
        user.password = "pass";
        Long id = userDao.save(user);
        task.name = "ivansTask" + System.currentTimeMillis();
        task.userId = id;
        Long taskId = taskDao.save(task);
        TaskEntity returnedTasks = taskDao.findById(taskId);
        assertEquals(returnedTasks, task);


    }










}
