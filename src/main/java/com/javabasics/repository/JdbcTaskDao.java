package com.javabasics.repository;

import com.javabasics.connection.ConnectionFactory;
import com.javabasics.repository.entity.TaskEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcTaskDao implements TaskDao {
    private Connection connection = ConnectionFactory.getConnection();



        @Override
        public Long save(TaskEntity taskEntity) {
            PreparedStatement ps = null;
            Statement statement = null;
            ResultSet resultSet = null;

            try {
                ps = connection.prepareStatement("insert into task(name,user_id) values(?,?)");
                ps.setString(1, taskEntity.name);
                ps.setLong(2, taskEntity.userId);
                ps.execute();
                statement = connection.createStatement();
                resultSet = statement.executeQuery("select LAST_INSERT_ID() as id");
                resultSet.next();
                return resultSet.getLong("id");

            } catch (SQLException e) {
                throw new RuntimeException();
            } finally {
                try {
                    statement.close();
                    ps.close();
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }

        }

    @Override
    public TaskEntity findById(Long id) {
        return null;
    }


    @Override
    public List<TaskEntity> findByUserId(Long userId) {
        Statement statement = null;

        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from task where user_id =" + userId);
            List<TaskEntity> tasks = new ArrayList<>();
            while (resultSet.next()) {
                TaskEntity taskEntity = new TaskEntity();
                taskEntity.id = resultSet.getLong("id");
                taskEntity.name = resultSet.getString("name");
                taskEntity.userId = resultSet.getLong("user_id");
                tasks.add(taskEntity);
            }
            return tasks;
        } catch (SQLException e) {
            throw new RuntimeException("");
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }
}
