package com.javabasics.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class Util {
    public static Class<?> getGenericTypeClassFromObjectByIndex(Object obj, int genericTypeIndex) {
        Type genericSuperClass = obj.getClass().getGenericSuperclass();
        ParameterizedType parametrizedType = null;
        while (parametrizedType == null) {
            if (genericSuperClass instanceof ParameterizedType) {
                parametrizedType = (ParameterizedType) genericSuperClass;
            } else {
                genericSuperClass = ((Class<?>) genericSuperClass).getGenericSuperclass();
            }
        }
        return (Class<?>) parametrizedType.getActualTypeArguments()[genericTypeIndex];
    }
}
