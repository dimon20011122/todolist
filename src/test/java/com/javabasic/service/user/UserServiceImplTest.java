package com.javabasic.service.user;

import com.javabasics.repository.JdbcUserDao;
import com.javabasics.repository.JpaUserDao;
import com.javabasics.repository.UserDao;
import com.javabasics.service.UserServiceImpl;
import com.javabasics.service.model.User;
import com.javabasics.service.user.UserService;
import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class UserServiceImplTest {
    private UserDao userDao = new JpaUserDao();
    private UserService userService = new UserServiceImpl(userDao);

    @Test
    public void idIsNotEmptyAfterSaving() {
        User user = new User();
        user.name = "ivan" + System.currentTimeMillis();
        user.password = "pass";
        Long id = userService.save(user);
        assertTrue(id != null && id != 0);
    }

    @Test
    public void userIsEqualWithReturnedById() {
        User user = new User();
        user.name = "ivan" + System.currentTimeMillis();
        user.password = "password";
        Long id = userService.save(user);
        User returnedUser = userService.findById(id);
        Assert.assertEquals(returnedUser, user);
    }

    @Test
    public void nameAndPasswordAreNotNullAfterReturnedByNameAndPassword() {
        User user = new User();
        user.name = "Ivan" + System.currentTimeMillis();
        user.password = "pass";
        userService.save(user);
        User returnedUser = userService.findByNameAndPassword(user.name, user.password);
        Assert.assertEquals(returnedUser, user);
    }





}
