package com.javabasic.service.user;

import com.javabasics.repository.JdbcTaskDao;
import com.javabasics.repository.JdbcUserDao;
import com.javabasics.repository.TaskDao;
import com.javabasics.repository.UserDao;
import com.javabasics.repository.entity.TaskEntity;
import com.javabasics.service.TaskService;
import com.javabasics.service.TaskServiceImpl;
import com.javabasics.service.UserServiceImpl;
import com.javabasics.service.model.Task;
import com.javabasics.service.model.User;
import com.javabasics.service.user.UserService;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;

public class TaskServiceImplTest {
    private TaskDao taskDao = new JdbcTaskDao();
    private UserDao userDao = new JdbcUserDao();
    TaskService taskService = new TaskServiceImpl(taskDao);
    private UserService userService = new UserServiceImpl(userDao);


    @Test
    public void taskIsSaving() {
        Task task = new Task();
        User user = new User();
        user.name = "ivan" + System.currentTimeMillis();
        user.password = "pass";
        Long id = userService.save(user);
        task.name = "ivansTask" + System.currentTimeMillis();
        task.userId = id;
        Long taskId = taskService.save(task);
        assertTrue(taskId != null && taskId != 0);
    }

    @Test
    public void findingByUserIdAndChekingNotNull() {
        Task task = new Task();
        User user = new User();
        user.name = "ivan" + System.currentTimeMillis();
        user.password = "pass";
        Long id = userService.save(user);
        task.name = "ivansTask" + System.currentTimeMillis();
        task.userId = id;
        Long taskId = taskService.save(task);
        Task returnedTasks = taskService.findByUserId(task.userId).get(0);
        Assert.assertEquals(returnedTasks, task);

    }
    @Test
    public void tasksAreExistAfterSavingForParticularUser() {
        User user = new User();
        user.name = "ivan" + System.currentTimeMillis();
        user.password = "pass";
        Long userId = userService.save(user);

        Task task1 = new Task();
        task1.name = "ivansTask" + System.currentTimeMillis();
        task1.userId = userId;
        Long taskId1 = taskService.save(task1);

        Task task2 = new Task();
        task2.name = "ivansTask" + System.currentTimeMillis();
        task2.userId = userId;
        Long taskId2 = taskService.save(task2);

        List<Task> tasks = taskService.findByUserId(userId);

        boolean isTask1Exist = false;
        boolean isTask2Exist = false;

        for (Task task: tasks) {
              if (task.id.equals(taskId1)) {
                  isTask1Exist = true;
              }
              if (task.id.equals(taskId2)) {
                  isTask2Exist = true;
              }


        }
        assertTrue(isTask1Exist  && isTask2Exist );


    }
}

