package com.javabasic.service.user;

import com.javabasics.repository.JpaUserDao;
import com.javabasics.repository.UserDao;
import com.javabasics.repository.entity.UserEntity;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JpaUserDaoTest {

    private UserDao userDao = new JpaUserDao();

    @Test
    public void JpaUserDaoIsFindingByNameAndPassword() {
        UserEntity user = new UserEntity();
        user.name = "ivan" + System.currentTimeMillis();
        user.password = "pass";
        Long id = userDao.save(user);
        UserEntity returnedUser = userDao.findByNameAndPassword(user.name, user.password);
        assertEquals(returnedUser, user);
    }
}
