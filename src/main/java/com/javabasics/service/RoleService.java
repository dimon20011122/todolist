package com.javabasics.service;


import com.javabasics.service.model.Role;

public interface RoleService {
    Long save(Role role);
    Role findByRoleId(Long id);
}
