package com.javabasics.service;

import com.javabasics.repository.TaskDao;

import com.javabasics.repository.entity.TaskEntity;
import com.javabasics.service.model.Task;

import java.util.ArrayList;
import java.util.List;


public class TaskServiceImpl implements TaskService {
    private TaskDao taskDao;




    public TaskServiceImpl(TaskDao taskDao){
        this.taskDao = taskDao;
    }



    @Override
    public Long save(Task task) {
        return taskDao.save(convertToTaskEntity(task));
    }


    private TaskEntity convertToTaskEntity(Task task) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.name=task.name;
        taskEntity.userId=task.userId;
        return taskEntity;
    }


    public List<Task> findByUserId(Long userId) {
        List<Task> tasks = new ArrayList<>(taskDao.findByUserId(userId).size());
        for(TaskEntity taskEntity: taskDao.findByUserId(userId)){
            tasks.add(convertToTask(taskEntity));
        }
        return tasks;
    }





    private Task convertToTask(TaskEntity taskEntity) {
        Task task = new Task();
        task.name=taskEntity.name;
        task.userId=taskEntity.userId;
        task.id=taskEntity.id;

        return task;
    }


}
