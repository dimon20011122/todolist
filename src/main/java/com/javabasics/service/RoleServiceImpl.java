package com.javabasics.service;

import com.javabasics.repository.RoleDao;
import com.javabasics.repository.entity.RoleEntity;
import com.javabasics.service.model.Role;


public class RoleServiceImpl implements RoleService {
    private RoleDao roleDao;




    public RoleServiceImpl(RoleDao roleDao){
        this.roleDao = roleDao;
    }



    @Override
    public Long save(Role role) {
        return roleDao.save(convertToRoleEntity(role));
    }


    private RoleEntity convertToRoleEntity(Role role) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.name=role.name;
        return roleEntity;
    }


    public Role findByRoleId(Long id) {
        return convertToRole(roleDao.findByRoleId(id));
    }





    private Role convertToRole(RoleEntity roleEntity) {
        Role role = new Role();
        role.name=roleEntity.name;
        role.id=roleEntity.id;

        return role;
    }

}
