package com.javabasics.service.model;

import java.util.Objects;

public class Task {
  public Long id;
  public String name;
  public Long userId;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Task task = (Task) o;
    return Objects.equals(name, task.name) &&
            Objects.equals(userId, task.userId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, userId);
  }
}
