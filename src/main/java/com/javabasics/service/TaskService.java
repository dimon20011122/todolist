package com.javabasics.service;

import com.javabasics.repository.entity.TaskEntity;
import com.javabasics.service.model.Task;

import java.util.List;

public interface TaskService {
    Long save(Task task);
    List<Task> findByUserId(Long userId);


}
