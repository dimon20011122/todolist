package com.javabasics.repository;

import com.javabasics.connection.ConnectionFactory;
import com.javabasics.repository.entity.TaskEntity;
import com.javabasics.util.Util;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GenericDao<T> {
    private Class<?> type = Util.getGenericTypeClassFromObjectByIndex(this, 0);
    private Connection connection = ConnectionFactory.getConnection();

    public Long save(T t) {
        Statement statement;
        ResultSet resultSet;
        String sqlToInsert = generateSqlToInsert(t);
        try {
            statement = connection.createStatement();
            statement.executeUpdate(sqlToInsert);
            resultSet = statement.executeQuery("select LAST_INSERT_ID() as id");
            resultSet.next();
            return resultSet.getLong("id");
        } catch (SQLException e) {
            throw new RuntimeException("The error occurred during saving");
        }
    }

    private String generateSqlToInsert(T t) {
        Field[] fields = t.getClass().getDeclaredFields();
        List<String> fieldNames = new ArrayList<>();
        List<String> fieldValues = new ArrayList<>();
        for (Field field : fields) {
            if (field.getAnnotation(Id.class) == null) {
                if (field.getAnnotation(Column.class) != null) {
                    fieldNames.add(field.getAnnotation(Column.class).value());
                } else {
                    fieldNames.add(field.getName());
                }
                try {
                    fieldValues.add(String.valueOf(field.get(t)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        fieldValues = fieldValues.stream().map(fieldValue -> "'" + fieldValue + "'").collect(Collectors.toList());
        String fieldNamesPlaceholder = String.join(",", fieldNames);
        String fieldValuesPlaceholder = String.join(",", fieldValues);
        return "insert into " + getTableName() + " (" + fieldNamesPlaceholder + ") values (" + fieldValuesPlaceholder + ")";
    }

    private String getTableName() {
        Table table = type.getAnnotation(Table.class);
        if (table != null) {
            return table.value();
        } else {
            return type.getSimpleName();
        }
    }


    public T findById(Long id) {
        Statement statement = null;
        ResultSet resultSet = null;
        Field[] fields = type.getDeclaredFields();
        T t = null;
        try {
            t = (T) type.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from " + getTableName() + " where id = " + id);
            resultSet.next();
            for (Field field : fields) {
                try {
                    field.set(t, resultSet.getObject(getColumnName(field)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }


            }
            return t;


        } catch (SQLException e) {
            throw new RuntimeException();
        } finally {
            try {
                statement.close();
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public String getColumnName(Field field) {
        Column column = field.getAnnotation(Column.class);
        if (column != null) {
            return column.value();
        } else {
            return field.getName();
        }
    }


    List<T> findByParameters(Map<String, Object> parameters) {
        Statement statement = null;
        List<T> entities = new ArrayList<>();

        String filter = parameters.entrySet()
                .parallelStream()
                .map(entry -> entry.getKey() + "=" + "'" + entry.getValue() + "'")
                .collect(Collectors.joining(" and "));
        String sql = "select * from " + getTableName() + " where " + filter;

        try {
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            Field[] fields = type.getDeclaredFields();
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                T t = null;
                try {
                    t = (T) type.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                }
                for (Field field : fields) {
                    field.set(t, rs.getObject(getColumnName(field)));

                }
                entities.add(t);
                {

                }
            }
            return entities;
        } catch (SQLException | IllegalAccessException e) {
            throw new RuntimeException("");
        }


    }


}
